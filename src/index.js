import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Routing from './component4/routing/routing';
import Counter from './component4/Counter/Counter';

import Crud from './component4/crudredux/Crud';
import CrudR from './component4/CrudR/crudR';

import User from './component6/User';

ReactDOM.render(
<div>
    <Routing />
    <Counter />
    <CrudR />
    <Crud />
</div>
, document.getElementById('root'));
ReactDOM.render( <User /> , document.getElementById("root"));



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

serviceWorker.unregister();
