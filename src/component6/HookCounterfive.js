import React , {useState , useEffect} from 'react'

function HookCounterfive() {
    const [count , setState] = useState(0);
    const [name , setName] = useState('');
    useEffect (()=>{
        console.log('updating document title ');
        document.title = `you clickd ${count} times `;
    },[count])
    return (
        <div>
            <input type = 'text'  value = {name} onChange = {(e)=>setName(e.target.value)} />
            <button onClick={()=>setState (count => count + 1)}>count {count}</button>
        </div>
    )
}

export default HookCounterfive
