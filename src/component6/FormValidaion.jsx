import React from "react";
// This for clearing the form
const initialState = {
  name: "",
  nameError: "",
};
const styleError = { fontSize: 18, color: "red" };
class FormValidaion extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  handleChange = e => {
    this.setState({
      [e.target.name] : e.target.value
    });
  };
  
  validate = () => {
    let nameError = "";
    if (this.state.name == "") {
      nameError = "Name Cant blank";
    } else if (this.state.name.length < 5) {
      nameError = "Name is to short";
    }
    if (nameError) {
      this.setState({ nameError });
      return false;
    }
    return true;
  };

  handleSubmit = () => {
    const isValid = this.validate();
    if (isValid) {
      console.log(`Name ${this.state.name}`);
      this.setState(initialState);
    }
  };
  render() {
    return (
      <div>
        <label>Name</label>
        <input name="name" value={this.state.name} onChange={this.handleChange} />
        <div style={styleError}>{this.state.nameError}</div>
        <br />
        <button onClick={this.handleSubmit}>Submit</button>
      </div>
    );
  }
}
export default FormValidaion;