import React , {useState}from 'react'

function HookCounterfour() {

    const [items , setItems] = useState([])
    const addItem = () => {
    setItems([...items , {  id : items.length , value : 2 } ]);
    }
    return (
        <div>
            <button onClick = {addItem}>Add Number</button>
            <ul>
                {
                items.map(item => (<li key = {item.id}> {item.value} </li>) )
                }
            </ul>

            
        </div>
    )
}

export default HookCounterfour
