import React from "react";
import axios from "axios";
class User extends React.Component {
  constructor() {
    super();
    this.state = {
      users: [],

      id: "",
      nm: "",
      mail: ""
    };
  }
  handleChangeEvent(evt) {
    this.setState({ [evt.target.name]: evt.target.value } );
  }

  handleSubmit(evt) {
    evt.preventDefault();
    const user = {
      id: this.state.id,
      name: this.state.nm,
      mail: this.state.mail
    };
    axios.post(`http://localhost:3001/users`, user).then(res => {
      let data = res.data;
      this.setState({
        users: [...this.state.users, data]
      });
    });
  }

  componentDidMount() {
    axios.get(`http://localhost:3001/users`).then(res => {
      // response.data is uesd to retrevie the data from response
      const users = res.data;
      this.setState({ users });
    });
  }
  render() {
    return (
      <div>
        <ul>
          {this.state.users.map((users, i) => (
            <li key={i}> {users.name} </li>
          ))}
        </ul>

        <h1>Add user</h1>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <label>User Id</label>
          <input
            type="text"
            name="id"
            onChange={this.handleChangeEvent.bind(this)}
          />
          <br />
          <label>User name</label>
          <input
            type="text"
            name="nm"
            onChange={this.handleChangeEvent.bind(this)}
          />

          <br />
          <label>User mail</label>
          <input
            type="text"
            name="mail"
            onChange={this.handleChangeEvent.bind(this)}
          />

          <button type="submit" value="submit">
            ADD
          </button>
        </form>
      </div>
    );
  }
}
export default User;