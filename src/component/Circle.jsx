/*Q1. Write a EcmaScript program to calculate the area and perimeter of a circle using arrow
function.

Note: Create two methods to calculate the area and perimeter. The radius of the circle will be
supplied by the user.*/



import React from 'react';
class Circle extends React.Component{

        constructor(props) {
            super(props);
            this.state = {
                area: 0,
                perimeter:0
            };
        }

    render(){
        const PI = 3.14;
        const calArea = (radious) => {
            let area = PI * (radious * radious);
            this.setState({area:area});
            console.log(`Area : ${area}`)
        }
        const calParimeter = (redious) => {
            let perimeter = 2 * PI * redious;
            this.setState({perimeter:perimeter});
            console.log(`Perimeter : ${perimeter}`)
        }
        return (
            <div>
               <input type="text" onChange = { e => { calArea(e.target.value); calParimeter(e.target.value);  }} />
               <p>Area and Perimeter of a circle </p> 
                Area : {this.state.area}   &nbsp; 
                Perimeter : : {this.state.perimeter}
            </div>
        );
    }
}

export default Circle;