/*
1. Create a React Routing application as per below given requirement
• It will render Home component which will print “Welcome to Home Page”
• On click of ContactUS link it will render Contact component
• Active link color set to green
*/

import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink
} from "react-router-dom";

import Home from "./Home";
import Contactus from "./Contactus";
class Routing extends React.Component {
  render() {
    return (
          <Router>
              <div>
                <b>Answer 1 </b><br />
                <h3>React Routing</h3>
                <ul>
                  <li>
                    <NavLink exact activeStyle={{ color: "green" }} to="/">
                      Home
                    </NavLink>
                  </li>

                  <li>
                    <NavLink exact activeStyle={{ color: "green" }} to="/contactus">
                    Contact us
                    </NavLink>
                  </li>
                </ul>
                <Route exact path="/" component={Home} />
                <Route exact path="/contactus" component={Contactus} />
              </div>
              <hr />
          </Router>
    );
  }
}
export default Routing;
