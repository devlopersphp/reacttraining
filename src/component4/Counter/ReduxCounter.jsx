import React from 'react';
import {connect } from 'react-redux';

class ReduxCounter extends React.Component{
    increment = () =>{
       this.props.dispatch({type : 'increment'})
    }

    decrement = () =>{
        this.props.dispatch({type : 'decrement'})
    }

    render(){
        return(
            <div>
                <b>Answer 2 </b><br />
                Counter : {this.props.count} <br />
                <button onClick={this.increment}> increment </button>
                <button onClick={this.decrement}> decrement </button>
            </div>
        );
    }
}

const mapStateToProps = (state) =>({
    count : state.count
});

export default connect (mapStateToProps) (ReduxCounter)
