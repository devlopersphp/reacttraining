import React from 'react';
import ReduxCounter from './ReduxCounter'

import {createStore} from 'redux';

import {Provider } from 'react-redux';
class Counter extends React.Component{

    render(){

        const initialState = {
            count : 0
        } 

        function reducer(state = initialState, action){
            console.log(action);
            switch (action.type){
                case 'increment' : 
                        return {
                           count : state.count + 1
                        };
                case 'decrement' : 
                return {
                    count : state.count - 1
                };        
                default : 
                        return state;
            }
        }
        const store = createStore(reducer);

        return (
           
            <Provider store = {store}> <ReduxCounter /> </Provider>
        );
    }

}
export default Counter 