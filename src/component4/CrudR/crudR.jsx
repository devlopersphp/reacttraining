import React from 'react';
let postdata =[];
class CrudR extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            name : '',
            email : '',
            formData : [], 

            editrow : false, 
            rownum : '',

            editname : '',
            editemail : '',

            saverow : false
        }

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
       if (event.target.getAttribute('id')){
            var index = event.target.getAttribute('id');
           
            this.setState({
                [event.target.editname] : event.target.value,
                [event.target.editemail] : event.target.value,
                saverow : true
            });
       }

        this.setState({
            [event.target.name]: event.target.value,
            [event.target.email]: event.target.value,
        });
    }

    handleSubmit = (e) => {
    e.preventDefault();
    postdata = [...this.state.formData];
    postdata.push({ name: this.state.name  , email : this.state.email});
        this.setState({
            formData : postdata ,
            name : '',
            email : ''
        });
    };
    
    deleteData = (e) =>{
        var index = e.target.getAttribute('id');
        postdata.splice(index, 1);
        this.setState({
            name : '',
            email : '',
            formData : postdata ,
        });
    };
    
    editData = (e)=>{
        var index = e.target.getAttribute('id');
        this.setState({
            editrow : true, 
            rownum : index,
            editname : postdata[index]['name'],
            editemail : postdata[index]['email'],
        });
    };

    saveData = (e) => {
        var index = e.target.getAttribute('id');
        postdata[index]['name'] = this.state.editname;
        postdata[index]['email'] = this.state.editemail;
        this.setState({
            saverow : false,
            editrow : false, 
            rownum : '',
            editname : '',
            editemail : ''
        });
    }
    
        showData(){
            if (postdata){
                return <table> {
                    postdata.map((item , index ) =>{
                    return  <tr>
                                {
                                (this.state.editrow  && this.state.rownum == index) 
                                ? 
                                <td> Name : <input id={index} name ='editname' type="text" value={this.state.editname} onChange={this.handleChange }  />  </td>
                                :
                                <td> Name : {item.name} </td>
                                }

                                {
                                (this.state.editrow  && this.state.rownum == index) 
                                ? 
                                <td> Email : <input id={index} name ='editemail' type="text" value={this.state.editemail} onChange={this.handleChange }  />  </td>
                                :
                                <td> Email : {item.email} </td>
                                }

                                {
                                (this.state.saverow  && this.state.rownum == index) 
                                ? 
                                <td>
                                    <button onClick={this.saveData} id = {index}>Save </button> 
                                </td>
                                :
                                <td>
                                <button onClick={this.editData} id = {index}> Edit </button> &nbsp;
                                <button onClick={this.deleteData} id = {index}> Delete </button>
                                </td>
                                }
                                    
                            </tr>;
                    })
                }
                </table>
            }
        }
        render(){
            return(
                <div>
                    <hr />
                    <b>Answer 3  Crud with Local State </b><br />
                    <form>
                        Name : <input name ='name' type="text" value={this.state.name} onChange={this.handleChange} /> <br />
                        email : <input name = 'email' type="text" value={this.state.email} onChange={this.handleChange} /> <br />
                        <button onClick={this.handleSubmit}>Submit</button>
                    </form>
                   {this.showData()}
                </div>
            );
        }
}
export default CrudR