import React, { Component } from 'react';
import PostForm from './PostForm';
import AllPost from './AllPost';

import { createStore } from 'redux';
import postReducer from './reducers/postReducer';
import { Provider } from 'react-redux';

class Crud extends Component {
    render() {
      const store = createStore(postReducer);
        return (
        <div >
          <hr />
          <b>Answer 4 Crud with Redux </b><br />
          <Provider  store={store}>
            <PostForm />
            <AllPost />
          </Provider>
        </div>
        );
    }
  }
export default Crud;