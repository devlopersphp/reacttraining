import React from "react";
class OrderControlled extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      orderId : "",
      orderName : "",
      orderDesc : ""
    };
  }
  handleChange = event => {
    this.setState({ name: event.target.value , orderId: event.target.value , orderName: event.target.value , orderDesc: event.target.value });
  };
 
  handleSubmit = event => {
    event.preventDefault();
    console.log(this.state);
  };

  render() {
    return (
      <div>
        Welcome to Customer Order Page
            <br />
            <label>Customer Name &nbsp;</label>
            <input onChange={this.handleChange} />
            <br />
            <label>Order Id &nbsp;</label>
            <input onChange={this.handleChange} />
            <br />
            <label>Order Name &nbsp;</label>
            <input onChange={this.handleChange} />
            <br />
            <label>Order Description &nbsp;</label>
            <input onChange={this.handleChange} />
            <br />
            <button onClick={this.handleSubmit}>Submit</button>
      </div>
    );
  }
}
export default OrderControlled;