
/*
1. Create a React statefull Custometr Component which renders Customer detsils
 Use following guidelines
a. Refer the state based Implemention
b. Set initial state as a JSON object
c. Change the state on onClick event
d. Refer below given outputs to understand information needed to render Customer
component
*/

import React from "react";
import OrderFC from './OrderFC';
import Order from './Order';
import Homepage from './mycomponent/Homepage';
import OrderControlled from './OrderControlled';
import OrderUncontrolled from './OrderUncontrolled';
class Customer extends React.Component {
  constructor(props) {
    
    super(props);
    
        this.state = {
            custcustId : "101",
            custName : "Shubhi",
            custAge : 24, 

            order : ''
        };  
    
    this.changeState = this.changeState.bind(this);
    this.updateState = this.updateState.bind(this);
    }


    changeState() {
        this.setState({ custId : "102",custName : "Riyan", custAge : 25 });
    }

    updateState() {

        let Orders = [
            {   orderId: "B001",
                orderName: 'Sports',
                orderDescription: 'Sports Rated Product'
            },
            {   orderId: "B002",
                orderName: 'Grocery',
                orderDescription : 'Grocery Realted Product'
            }
        ];
       
        let OrderData = Orders[Math.floor(Math.random() * 2)];
        this.setState({order : OrderData });
     
    }
    showOrder(){
        if (this.state.order != ''){
            return <OrderFC  order = {this.state.order} />
        }
    }


    render() {
      
        return (
        <div>
            <b>Answer 1. Customer Details</b>
            <ul>
            <li>cust Id  : {this.state.custId}</li>
            <li>cust Name : {this.state.custName}</li>
            <li>cust Age : {this.state.custAge}</li>
            </ul>
            <button onClick = {this.changeState}>Change State</button>
            <hr />
            <b>Answer 2. Random Customer Order Details </b>
            <ul>
            <li>cust Id  : {this.state.custId}</li>
            <li>cust Name : {this.state.custName}</li>
            <li>cust Age : {this.state.custAge}</li>
            <li>cust Address : Noida</li>
            </ul>
            {this.showOrder()}
            <button onClick = {this.updateState}>Add Random Order</button>
            <hr />
            
            <b>Answer 3. Order Details of Neetya </b>            
            <Order />
            <hr />
            
            <b>Answer 4. </b>            
            <Homepage />
            <hr />
            
            <b>Answer 5. </b>            
            <OrderControlled />
            <hr />
            
            <b>Answer 6. </b>            
            <OrderUncontrolled />
        </div>

        );
    }
}

export default Customer;