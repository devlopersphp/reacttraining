
/*
3. Create a React application using following below given guidelines
a. Create a statefull Order component which contains order information as a JSON
object.
b. Inside Order component declare addEventHandler and showEventHandler method
c. The addEventHandler add order information to the state object and display a popup
message Order Added Successfully!!
d. The showEventHandler display the order information
e. Create a Customer Component which render customer name
f. Use Order component as a Nested Component of Customer component
g. Refer the state based Implemention
h. Refer below given outputs to understand information needed to render Customer and
Order components
*/

import React from 'react'
class Order extends React.Component {

    constructor(props) {
        super(props);
        
            this.state = {
                order : ''
            };  
        
        this.addEventHandler = this.addEventHandler.bind(this);
        this.showEventHandler = this.showEventHandler.bind(this);
        }

    addEventHandler(){
        let OrderData = {   orderId: "B001",
            orderName: 'Sports',
            orderDescription: 'Sports Rated Product'
        }
        this.setState({  order : OrderData });
        alert("Order Added Successfully");
    }

    showEventHandler(){
        if (this.state.order != ''){
           // document.write(`Order Data : ${this.state.order}`);
            document.getElementById('orderdata').innerHTML = `
            <ul> Order Data
                <li> Order Id :  ${this.state.order.orderId} </li>
                <li> Order Name :  ${this.state.order.orderName} </li>
                <li> Order Description :  ${this.state.order.orderDescription} </li>
            </ul>
            <p>====================================</p>
            `;
            console.log(`Order Data : ${this.state.order}`)
        }
    }

    render (){
        return (
            <div>
            <div id = "orderdata"></div>
            <button onClick = {this.addEventHandler}>Add</button>
            <button onClick = {this.showEventHandler}>Show</button>
            </div>
        );
    }

}

export default Order 
