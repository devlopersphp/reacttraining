/*
2. Create a React application using following below given guidelines
a. Create a Functional component Order which renders order information passed from
Customer component
b. Create a statefull Customer component which display customer information form its
state
c. Declare an eventhandler method inside Cutomer component which contains array of
orders object it will push a random order object inside the array and updated the state
with that order object which than need to be passed inside Order component to be
rendered
d. Refer the state and props based Implemention
e. Refer below given outputs to understand information needed to render Customer and
Order components
*/

import React from "react";
const OrderFC = (props) => {

  return (
    <div>
       <ul> Order Data : 
        <li> Order Id : {props.order.orderId} </li>
        <li> Order Name : {props.order.orderName} </li>
        <li> Order Description : {props.order.orderDescription} </li>
        </ul>
        <p>====================================</p>
    </div>
  );

};

export default OrderFC;