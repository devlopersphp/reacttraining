import React from "react";
class OrderUncontrolled extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            custName : '',
            OrderId : '',
            OrderName : '',
            OrderDesc : ''
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(e) {
        
        e.preventDefault();

        let custName = this.refs.custName.value;
        let OrderId = this.refs.OrderId.value;
        let OrderName = this.refs.OrderName.value;
        let OrderDesc = this.refs.OrderDesc.value;
        
        this.setState({
            custName : custName,
            OrderId : OrderId,
            OrderName : OrderName,
            OrderDesc : OrderDesc
        });

        console.log(
            `Customer Name : ${custName}
            Order Id : ${OrderId}
            Order Name : ${OrderName}
            Order Description : ${OrderDesc}`
        );
    }

    showData=()=>{
        document.getElementById('orderData').innerHTML  = `<br /> Customer Name : ${this.state.custName}
        <br />Order Id : ${this.state.OrderId}
        <br />Order Name : ${this.state.OrderName}
        <br />Order Description : ${this.state.OrderDesc}`
        ; 
    }

    render() {
        return (
        <div>
            <form onSubmit={this.handleSubmit}>
            Customer Name
            <input name="custName" ref="custName" />
            <br />
            Order Id
            <input name="OrderId" ref="OrderId" />
            <br />
            Order Name
            <input name="OrderName" ref="OrderName" />
            <br />
            Order Description
            <input name="OrderDesc" ref="OrderDesc" />
            <br />
            <input type="submit" value="Submit" />
            </form>

           <button onClick = {this.showData}>Show Order Data</button>
           <div id="orderData"></div>
        </div>
        );
    }
}

export default OrderUncontrolled;