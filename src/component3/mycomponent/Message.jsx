import React from 'react';
class Message extends React.Component{

    constructor (props){
        super(props)
    }

    showMessage(){
        let user = '';
        if (this.props.isLoggedIn){
             user =  'Welcome User ';
        }else {
             user =  'Please Login ';
        }
        return user;
    }
    
    render() {
        return (
            <div>
                {this.showMessage()}                
            </div>
        )
    }


}
export default Message