import React from 'react'
import Message from './Message';
import Login from './Login';
import Logout from './Logout';

class Homepage extends React.Component{
    constructor(){
        super()
        this.state={
            isLoggedIn: '',
        }   
        //this.loginUser = this.loginUser.bind(this);
        //this.logoutUser = this.logoutUser.bind(this);
    }

    loginUser=()=>{
        this.setState({
            isLoggedIn: true
        });
    }

    logoutUser=()=>{
        this.setState({
            isLoggedIn: false
        });
    }

    render(){

        let showComponent;
        if(!this.state.isLoggedIn){
            showComponent=<Login onClick={this.loginUser} />
        }else{
            showComponent= <Logout onClick={this.logoutUser} />
        }

        return (
            <div>
                <h3>Welcome to Homepage !!!</h3>    
                <Message isLoggedIn = {this.state.isLoggedIn} />
                {showComponent}
            </div>
        );

    }


}
export default Homepage