/* Q1.
1. Create the React Custometr Component which renders Customer detsils passed as
input Use following guidelines
a. Refer the props based Implemention
b. Validate the input type using propTypes
c. Print “Defaut” as output if input is not provided
d. Refer below given output to understand information needed to render Customer
component
*/

import React from 'react';
import PropTypes from "prop-types";
import CustomerOrder from './CustomerOrder';

class Customer extends React.Component{

    render(){
        let custumerDetails={  
            custId:"C102c",
            custName:"Riyan",
            custAge:"24",
            custAddress :"Noida",
        };
        
        let custumerOrders = [
            {   orderId: "101",
                orderName: 'Dessert',
                orderDescription: 'choco lava'
            },
            {   orderId: "102",
                orderName: 'Dessert',
                orderDescription : 'caremal custard'
            },
        ];

        return(
            <div>
                <b>Answer 1 </b><br />
                <br /> Custumer Details  <br /><br />
                Customer Id :{this.props.custId} <br />
                Customer Name :{this.props.custName} <br />
                Customer Age :{this.props.custAge} <br />
                Customer Address :{this.props.custAddress} <br />
                <hr />
            <CustomerOrder  custumerDetails = {custumerDetails}  custumerOrders = {custumerOrders} />
            </div>
        );
    }
}

//default prop
Customer.defaultProps = {
    custId: "C10C1",
    custName: "Shubhi",
    custAge: 24,
    custAddress: "Noida"
};

//validating
Customer.propTypes = {
    custId: PropTypes.string,
    custName: PropTypes.string,
    custAge: PropTypes.number,
    custAddress: PropTypes.string
  };


export default Customer