/* Q2.
2. Create the React Custometr Component which contains the order information in
JSON Object.
 Use following guidelines
a. Pass customer details as input in the form JSON object
b. Render the customer details with the order description
c. Refer the props based Implemention
d. Refer below given output to understand information needed to render Customer and
Order components
*/

import React from 'react'
import { validate } from '@babel/types';
class CustomerOrder extends React.Component{

    constructor(props){
        super(props);
        
    }

    getcustumerOrders(custumerOrders) {
        if (custumerOrders) {
            return Object.keys(custumerOrders).map( (key) => {
                return <ul>
                            <li> orderId : {custumerOrders[key]['orderId']} </li>
                            <li> orderName : {custumerOrders[key]['orderName'] } </li>
                            <li> orderDescription : {custumerOrders[key]['orderDescription']} </li>
                        </ul>;
            });
        } else {
            return <p>data is not available</p>;
        }
    }
  

    render(){
        
        

        return(
            <div>
                
                <b> Answer 2 </b>
                <br /> Custumer Details  <br /><br />
                Customer Id :  {this.props.custumerDetails.custId} <br />
                customer Name : {this.props.custumerDetails.custName} <br />
                custAge : {this.props.custumerDetails.custAge} <br />
                custAddress : {this.props.custumerDetails.custAddress} <br />
                <hr />
                
                <b> Answer 3 </b>
                {this.getcustumerOrders( this.props.custumerOrders )}

            </div> 
        );
    }

}

export default CustomerOrder