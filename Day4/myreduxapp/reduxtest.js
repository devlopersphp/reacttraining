const { createStore } = require("redux");

const initialState = {
  sal: 20000
};

const myReducer = (state = initialState, action) => {
  const newState = { ...state };

  if (action.type == "Inc-Sal") {
    newState.sal += action.payLoad;
  }

  if (action.type == "Dec-Sal") {
    newState.sal -= action.payLoad;
  }

  return newState;
};

const store = createStore(myReducer);

store.subscribe(() => {
  console.log("State changed " + JSON.stringify(store.getState()));
});

store.dispatch({
  type: "Inc-Sal",
  payLoad: 2000
});

//console.log(store.getState());

store.dispatch({
  type: "Dec-Sal",
  payLoad: 1000
});

//console.log(store.getState());
