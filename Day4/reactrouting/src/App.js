import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink
} from "react-router-dom";
import Home from "./components/home";
import Product from "./components/product";
class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <h1>React Routing</h1>
          <ul>
            <li>
              <NavLink exact activeStyle={{ color: "green" }} to="/">
                Home
              </NavLink>
            </li>

            <li>
              <NavLink exact activeStyle={{ color: "green" }} to="/product">
                Product
              </NavLink>
            </li>
          </ul>

          <Route exact path="/" component={Home} />
          <Route exact path="/product" component={Product} />
        </div>
      </Router>
    );
  }
}
export default App;
