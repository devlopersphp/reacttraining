var utils = {
  disp: function() {
    return "Export default";
  },

  totalSal: function(salary, bonus) {
    return salary + bonus;
  }
};

export default utils;
