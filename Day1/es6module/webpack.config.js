module.exports = {
  entry: ["./src/index.js"],
  output: {
    filename: "bundle.js"
  },
  module: {
    loaders: [
      {
        loader: "babel-loader",
        test: /\.js$/
      }
    ]
  },

  devServer: {
    port: 3000
  }
};
