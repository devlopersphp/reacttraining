import React, { Component } from "react";
import AddPerson from "./components/AddPerson.jsx";
import PersonList from "./components/PersonList.jsx";
class App extends Component {
  render() {
    return (
      <div>
        <h1>Rest Example</h1>
        <PersonList />
        <AddPerson />
      </div>
    );
  }
}
export default App;
