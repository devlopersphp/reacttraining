import React from "react";
import axios from "axios";
export default class PersonList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: []
    };
  }

  componentDidMount() {
    axios
      .get(`https://jsonplaceholder.typicode.com/users`)
      // axios
      //   .get(`http://localhost:8082/RestProjectAPI/projservice/getJson`)
      //
      .then(res => {
        const persons = res.data;
        //const persons = res.data.project;
        this.setState({ persons });
      });
  }

  render() {
    return (
      <div>
        <h1>Axois Demo</h1>
        <ul>
          {this.state.persons.map((person, i) => (
            <li key={i}> {person.name}</li>
          ))}
        </ul>
      </div>
    );
  }
}
