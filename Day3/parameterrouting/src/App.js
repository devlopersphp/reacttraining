import React, { Component } from "react";
import { Link, Route, Switch } from "react-router-dom";
import Books from "./components/Books";
class App extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-light">
          <ul className="nav navbar-nav">
            <li className="nav-item">
              <Link to="/">Homes</Link>
            </li>
            <li className="nav-item">
              <Link to="/books">Books</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/books" component={Books} />
        </Switch>
      </div>
    );
  }
}

const Home = props => (
  <div>
    <h2>Home</h2>
  </div>
);

export default App;
