export const booksData = [
  {
    id: 1,
    name: "Java For Beginners",
    description: "A Complete book for Java Larners",
    status: "Available"
  },
  {
    id: 2,
    name: "Pro React",
    description: "Learn React step by step",
    status: "Available"
  },

  {
    id: 3,
    name: "Python for Data Analysis ",
    description: "Data Analysis using Python",
    status: "Out of Stock"
  }
];
