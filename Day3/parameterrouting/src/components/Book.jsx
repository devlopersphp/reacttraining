import React from "react";
const Book = ({ match, data }) => {
  var book = data.find(b => b.id === Number(match.params.bookId));
  var bookData;
  if (book)
    bookData = (
      <div>
        <h3> {book.name} </h3>
        <p>{book.description}</p>
        <hr />
        <h4>{book.status}</h4>{" "}
      </div>
    );
  else bookData = <h2> Sorry. Book doesnt exist </h2>;
  return (
    <div style={{ display: "flex" }}>
      <div
        style={{
          padding: "0 10% 0 10%",
          width: "80%",
          margin: "auto",
          background: "#ffffff"
        }}
      >
        {bookData}
      </div>
    </div>
  );
};

export default Book;
