import React from "react";
import { Link, Route } from "react-router-dom";
import Book from "./Book";
import { booksData } from "./booksdata";
const Books = ({ match }) => {
  // const booksData = [
  //   {
  //     id: 1,
  //     name: "Java For Beginners",
  //     description: "A Complete book for Java Larners",
  //     status: "Available"
  //   },
  //   {
  //     id: 2,
  //     name: "Pro React",
  //     description: "Learn React step by step",
  //     status: "Available"
  //   },

  //   {
  //     id: 3,
  //     name: "Python for Data Analysis ",
  //     description: "Data Analysis using Python",
  //     status: "Out of Stock"
  //   }
  // ];

  var linkList = booksData.map(book => {
    return (
      <li key={book.id}>
        <Link to={`${match.url}/${book.id}`}>{book.name}</Link>
      </li>
    );
  });

  return (
    <div>
      <div style={{ display: "block", justifyContent: "space-between" }}>
        <div
          style={{
            float: "left",
            padding: "10px",
            width: "30%",
            background: "#f0f0f0",
            marginLeft: "auto"
          }}
        >
          <h3> Books</h3>
          <ul style={{ listStyleType: "none", padding: 0, fontSize: "15px" }}>
            {linkList}
          </ul>
        </div>
      </div>
      <Route
        path={`${match.url}/:bookId`}
        render={props => <Book data={booksData} {...props} />}
      />
    </div>
  );
};

export default Books;
