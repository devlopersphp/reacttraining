const { createStore } = require("redux");

const initialState = {
  sal: 20000
};

const myReducer = (state = initialState, action) => {
  const newState = { ...state };
  if (action.type == "Inc-Sal") {
    newState.sal += action.payload;
  }

  if (action.type == "Dec-Sal") {
    newState.sal -= action.payload;
  }
  return newState;
};

const store = createStore(myReducer);

store.subscribe(() => {
  console.log(" Changed state " + JSON.stringify(store.getState()));
});

//console.log(store.getState());

store.dispatch({
  type: "Inc-Sal",
  payload: 5000
});

store.dispatch({
  type: "Dec-Sal",
  payload: 2000
});

//console.log(store.getState());
