import React from "react";
import Home from "./components/home";
class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Welcome to ReactJS</h1>
        <Home value={"Select the Link"} />
      </div>
    );
  }
}
export default App;
