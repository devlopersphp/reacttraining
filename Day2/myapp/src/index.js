import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Greeting from "./components/greeting";
import Employee from "./components/employee";
let name = "Ravic";
let age = 40;
let emp = {
  name: "Ravic",
  city: "Noida"
};
ReactDOM.render(<App />, document.getElementById("root"));
ReactDOM.render(
  <Greeting name={name} age={age} />,
  document.getElementById("root1")
);
